// Настройки ленты
#define NUM_LEDS 437   // количество светодиодов
#define LED_PIN 13     // пин ленты
#define COLOR_DEBTH 3  // цветовая глубина: 1, 2, 3 (в байтах)
// Настройки пламени
#define PERLIN_SCALE 5         // масштаб по перлину
#define PERLIN_SPEED 10        // скорость по перлину
//Красный
#define R_MIN_FROM 0
#define R_MIN_TO 0
#define R_MAX_FROM 255
#define R_MAX_TO 255
//Зеленый
#define G_MIN_FROM 96
#define G_MIN_TO 0
#define G_MAX_FROM 255
#define G_MAX_TO 128
//Синий
#define B_MIN_FROM 230
#define B_MIN_TO 0
#define B_MAX_FROM 255
#define B_MAX_TO 80

#include <microLED.h>
#include <FastLED.h>

microLED<NUM_LEDS, LED_PIN, MLED_NO_CLOCK, LED_WS2815, ORDER_RGB, CLI_HIGH> strip;
static uint32_t prevTime;
int perlinOffset;

void setup() {
  strip.setBrightness(250);
  strip.clear();
  strip.show();
  delayMicroseconds(100);
}

void loop() {
  //Отрисовываем огонь
  perlinOffset += PERLIN_SPEED;
  for (int i = 0; i < NUM_LEDS; i++) {
    strip.leds[i] = getFireColor((inoise8(i * PERLIN_SCALE, perlinOffset)));
  }

  //Oтображаем и ждем минимум 10МС
  strip.show();
  while ((millis() - prevTime) < 10) {
    delayMicroseconds(1);
  }
  prevTime = millis();
}


mData getFireColor(int val) {
  int r = 0;
  if (val >= R_MIN_FROM) {
    r = map(val, R_MIN_FROM, R_MAX_FROM, R_MIN_TO, R_MAX_TO);
  }
  int g = 0;
  if (val >= G_MIN_FROM) {
    g = map(val, G_MIN_FROM, G_MAX_FROM, G_MIN_TO, G_MAX_TO);
  }
  int b = 0;
  if (val >= B_MIN_FROM) {
    b = map(val, B_MIN_FROM, B_MAX_FROM, B_MIN_TO, B_MAX_TO);
  }
  return mRGB(r, g, b);
}
